# Summary
Ansible role to downlad and install Gitlab Runner.

Useful to avoid using gitlab.com shared runners:
- bypass free tier CI limits
- run your build/test/release in a more trusted environment
- increase build/test/release performance by running on a beefier host/VM

# Platforms
Ubuntu 22.04